use crate::artemis::{ArtemisConnection, Course};
use crate::clone::{clone_exercise, CloneConfiguration};
use crate::concurrent_stream::ConcurrentStream;
use crate::config::{CommonCourseConfig, Config, CourseConfig};
use crate::readme::generate_readme;
use crate::{
    artemis, format_datetime_locale, format_duration, Cli, Error, COLOR_DEADLINE_CLOSE,
    COLOR_DEADLINE_ENDED, COLOR_DEADLINE_FAR, COLOR_SHORT_NAME, COLOR_TEAM,
};
use chrono::{Duration, Utc};
use clap::{Parser, ValueHint};
use futures::StreamExt;
use std::collections::HashMap;
use std::path::PathBuf;

/// Print your information about an exercise.
#[derive(Debug, Parser)]
pub struct Exercise {
    #[clap(value_hint = ValueHint::Other)]
    course: String,

    #[clap(value_hint = ValueHint::Other)]
    exercise: String,
}

impl Exercise {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_none() {
            println!("You are logged out. Login using `pandia login`.");
            return Ok(false);
        }

        let courses = conn.courses().await?;
        let course = get_course(&conn, &self.course, courses.as_ref())?;

        let exercise = get_exercise(&conn, &self.exercise, course)?;
        let exercise = conn.exercise(exercise.id).await?;

        let mut s = String::new();
        generate_readme(&conn, &mut s, &exercise, None).await?;
        println!("{}", s);

        Ok(true)
    }
}

/// List all exercises.
#[derive(Debug, Parser)]
pub struct Exercises {
    /// Include exercises that are not included in your final score.
    #[clap(short = 't', long)]
    included_tutorials: bool,

    /// Show full dates instead of remaining time.
    #[clap(short = 'd', long)]
    full_date: bool,

    /// Show all exercises including completed ones.
    #[clap(short, long)]
    all: bool,
}

impl Exercises {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_none() {
            println!("You are logged out. Login using `pandia login`.");
            return Ok(false);
        }

        let courses = conn.courses().await?;

        let skip_cond = |e: &artemis::Exercise| {
            !self.all
                && (e.ended()
                    || !self.included_tutorials && &e.included_in_overall_score == "NOT_INCLUDED")
        };

        for course in &courses {
            if course.exercises.iter().all(skip_cond) {
                continue;
            }

            println!(
                "{} {}({}){}",
                course.title,
                COLOR_SHORT_NAME.prefix(),
                course.short_name,
                COLOR_SHORT_NAME.suffix()
            );
            for exercise in &course.exercises {
                if skip_cond(exercise) {
                    continue;
                }
                print!("  {}", exercise.title,);
                if exercise.team_mode {
                    print!(" ({})", COLOR_TEAM.paint("Team"))
                }
                if let Some(due_date) = exercise.due_date {
                    let ends_in = due_date - Utc::now();
                    let color_deadline = if ends_in <= Duration::zero() {
                        COLOR_DEADLINE_ENDED
                    } else if ends_in <= Duration::days(3) {
                        COLOR_DEADLINE_CLOSE
                    } else {
                        COLOR_DEADLINE_FAR
                    };
                    let s = if self.full_date {
                        format_datetime_locale(due_date).to_string()
                    } else {
                        format_duration(ends_in).to_string()
                    };
                    print!(" ({})", color_deadline.paint(s))
                }
                if let Some(short_name) = exercise.short_name.as_ref() {
                    print!(
                        " {}({}){}",
                        COLOR_SHORT_NAME.prefix(),
                        short_name,
                        COLOR_SHORT_NAME.suffix()
                    );
                }
                println!();
            }
            println!()
        }

        Ok(true)
    }
}

/// Clone exercises.
#[derive(Debug, Parser)]
pub struct Clone_ {
    #[clap(value_hint = ValueHint::Other)]
    course: String,

    #[clap(value_hint = ValueHint::Other)]
    exercises: Vec<String>,
}

impl Clone_ {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_none() {
            println!("You are logged out. Login using `pandia login`.");
            return Ok(false);
        }

        let courses = conn.courses().await?;
        let course = get_course(&conn, &self.course, courses.as_ref())?;

        let mut course_cfg = None;
        for some_course_cfg in cfg.courses {
            if course.short_name == some_course_cfg.name {
                course_cfg = Some(some_course_cfg);
            }
        }

        let course_cfg = course_cfg.unwrap_or_else(|| CourseConfig {
            name: course.short_name.to_string(),
            dir: "".to_string(),
            remote_bases: HashMap::default(),
            common: CommonCourseConfig {
                use_ssh: None,
                create_readme: None,
                commit_initial: None,
                tag_initial: None,
            },
        });

        let mut out_dir = PathBuf::from(&cfg.base_path);
        out_dir.push(course_cfg.dir);

        let committer = if cfg.committer.name.is_none() || cfg.committer.email.is_none() {
            let git_cfg = git2::Config::open_default()?;
            (
                cfg.committer.name.unwrap_or_else(|| {
                    git_cfg
                        .get_str("user.name")
                        .expect("no user name set")
                        .to_string()
                }),
                cfg.committer.email.unwrap_or_else(|| {
                    git_cfg
                        .get_str("user.email")
                        .expect("no email name set")
                        .to_string()
                }),
            )
        } else {
            (cfg.committer.name.unwrap(), cfg.committer.email.unwrap())
        };

        let cfg = CloneConfiguration {
            out_dir,
            use_ssh: course_cfg.common.use_ssh(&cfg.default),
            create_readme: course_cfg.common.create_readme(&cfg.default),
            commit_initial: course_cfg.common.commit_initial(&cfg.default),
            tag_initial: course_cfg.common.tag_initial(&cfg.default),
            committer_name: committer.0,
            committer_mail: committer.1,
            remote_bases: course_cfg.remote_bases,
        };

        let info = conn.info().await?;

        let mut tasks = Vec::with_capacity(self.exercises.capacity());
        if self.exercises.iter().any(|exercise| exercise == "all") {
            for exercise in &course.exercises {
                tasks.push(Box::pin(clone_exercise(&conn, &cfg, &info, exercise.id)))
            }
        } else {
            for exercise in &self.exercises {
                let exercise = get_exercise(&conn, exercise, course)?;
                tasks.push(Box::pin(clone_exercise(&conn, &cfg, &info, exercise.id)))
            }
        }

        let mut tasks = ConcurrentStream::new(tasks, 3);

        let mut results = Vec::with_capacity(self.exercises.capacity());
        while let Some(res) = StreamExt::next(&mut tasks).await {
            if let Err(err) = res {
                // TODO which exercise
                results.push(err);
            }
        }

        if !results.is_empty() {
            println!("Errors:")
        }
        for err in &results {
            println!("- {}", err);
        }

        Ok(results.is_empty())
    }
}

fn get_course<'a>(
    _conn: &ArtemisConnection,
    search: &str,
    courses: &'a [Course],
) -> Result<&'a Course, Error> {
    let search = search.to_lowercase();
    for course in courses {
        if course.short_name.to_lowercase() == search {
            return Ok(course);
        }
    }
    Err(Error::UnknownCourse { search })
}

fn get_exercise<'a>(
    _conn: &ArtemisConnection,
    search: &str,
    course: &'a Course,
) -> Result<&'a artemis::Exercise, Error> {
    let search = search.to_lowercase();
    for exercise in &course.exercises {
        if let Some(short_name) = &exercise.short_name {
            if short_name.to_lowercase() == search {
                return Ok(exercise);
            }
        }
    }
    Err(Error::UnknownExercise { search })
}
