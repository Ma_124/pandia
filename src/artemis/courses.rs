use crate::artemis::{ArtemisConnection, ArtemisError};
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct Course {
    pub id: u32,
    pub title: String,
    #[serde(default)]
    pub description: String,
    pub short_name: String,
    pub semester: String,
    #[serde(default)]
    pub course_icon: String,
    pub presentation_score: f32,
    #[serde(default)]
    pub exercises: Vec<Exercise>,
    #[serde(default)]
    pub lectures: Vec<Lecture>,
    // ...
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct Exercise {
    pub type_: String,
    pub id: u32,
    pub title: String,
    pub short_name: Option<String>,
    pub release_date: Option<DateTime<Utc>>,
    pub due_date: Option<DateTime<Utc>>,
    pub max_points: f64,
    pub bonus_points: f64,
    #[serde(default)]
    pub difficulty: String,
    pub mode: String,
    #[serde(default)]
    pub programming_language: String,
    #[serde(default)]
    pub problem_statement: String,
    pub course: Option<Course>,
    #[serde(default)]
    pub student_participations: Vec<StudentParticipation>,
    #[serde(default)]
    pub included_in_overall_score: String,
    #[serde(default)]
    pub team_mode: bool,
    // ...
}

impl Exercise {
    pub fn ended(&self) -> bool {
        if let Some(due_date) = self.due_date {
            Utc::now() >= due_date
        } else {
            false
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct Lecture {
    pub id: u32,
    pub title: String,
    pub description: String,
    pub attachments: Vec<Attachment>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct Attachment {
    pub id: u32,
    pub name: String,
    pub link: String,
    pub version: u32,
    pub attachment_type: String,
    // ...
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct StudentParticipation {
    #[serde(default)]
    pub repository_url: String,
    #[serde(default)]
    pub build_plan_id: String,
    pub participant_name: String,
    pub exercise: Option<Exercise>,
    // ...
}

impl ArtemisConnection {
    pub async fn courses(&self) -> Result<Vec<Course>, ArtemisError> {
        self.http_get("/api/courses/for-dashboard").await
    }

    pub async fn exercise(&self, exercise: u32) -> Result<Exercise, ArtemisError> {
        self.http_get(&format!("/api/exercises/{}/details", exercise))
            .await
    }

    pub async fn participate(
        &self,
        course: u32,
        exercise: u32,
    ) -> Result<StudentParticipation, ArtemisError> {
        self.http_post(
            &format!(
                "/api/courses/{}/exercises/{}/participations",
                course, exercise
            ),
            &"",
        )
        .await
    }
}
