use crate::config::Config;
use crate::{Cli, Error};
use clap::Parser;

/// Output information about Artemis.
#[derive(Debug, Parser)]
pub struct Info {}

impl Info {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;

        let info = conn.info().await?;

        println!(
            "{} ({}.{}) v{} built at {}",
            info.build.name,
            info.build.group,
            info.build.artifact,
            info.build.version,
            info.build.time
        );
        println!();
        println!("Programming Languages:");
        for lang in info.programming_language_features {
            println!("  {}", lang.programming_language);
            println!("    Sequential Test Runs:  {}", lang.sequential_test_runs);
            println!("    Static Code Analysis:  {}", lang.static_code_analysis);
            println!(
                "    Plagiarism Check:      {}",
                lang.plagiarism_check_supported
            );
            println!("    Package Name Required: {}", lang.package_name_required);
            if !lang.project_types.is_empty() {
                println!(
                    "    Project Types:         {}",
                    lang.project_types.join(", ")
                );
            }
            println!();
        }
        println!("SSH clone are relative to {}", info.ssh_clone_url_template);
        println!();
        println!("Browse VCS at {}", info.version_control_url);
        println!("Configure SSH keys at {}", info.ssh_keys_url);

        Ok(true)
    }
}

/// List your courses.
#[derive(Debug, Parser)]
pub struct Courses {}

impl Courses {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_none() {
            println!("You are logged out. Login using `pandia login`.");
            return Ok(false);
        }

        let courses = conn.courses().await?;
        for course in courses {
            println!("{} ({})", course.title, course.short_name);
            println!("  {}/courses/{}", cfg.artemis_url, course.id);
            println!("  Semester: {}", course.semester);
            if !course.description.is_empty() {
                println!("  Description:");
                println!("    {}", course.description.replace("\n", "\n    ").trim());
            }
            println!()
        }

        Ok(true)
    }
}

/// Print the server time.
#[derive(Debug, Parser)]
pub struct Time {}

impl Time {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;

        println!("{}", conn.time().await?);
        Ok(true)
    }
}

/// Print your account information.
#[derive(Debug, Parser)]
pub struct WhoAmI {}

impl WhoAmI {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_none() {
            println!("You are logged out. Login using `pandia login`.");
            return Ok(false);
        }

        let account = conn.account().await?;

        println!("{} <{}> [{}]", account.name, account.email, account.login);

        Ok(true)
    }
}
