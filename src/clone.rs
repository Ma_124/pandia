use crate::artemis::{ArtemisConnection, ArtemisInfo, StudentParticipation};
use crate::readme::generate_readme;
use crate::Error;
use git2::build::RepoBuilder;
use git2::{Cred, CredentialType, FetchOptions, ObjectType, RemoteCallbacks, Signature};
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::ops::Deref;
use std::path::PathBuf;
use tokio::task;

/// A [`::std::borrow::Cow`] that does not require [`::std::borrow::ToOwned`].
enum MaybeBorrowed<'a, T> {
    Owned(T),
    Borrowed(&'a T),
}

impl<'a, T> Deref for MaybeBorrowed<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        match self {
            MaybeBorrowed::Owned(t) => t,
            MaybeBorrowed::Borrowed(t) => *t,
        }
    }
}

pub struct CloneConfiguration {
    pub out_dir: PathBuf,
    pub use_ssh: bool,
    pub create_readme: bool,
    pub commit_initial: bool,
    pub tag_initial: bool,
    pub committer_name: String,
    pub committer_mail: String,
    pub remote_bases: HashMap<String, String>,
}

pub async fn clone_exercise(
    conn: &ArtemisConnection,
    cfg: &CloneConfiguration,
    info: &ArtemisInfo,
    exercise_id: u32,
) -> Result<(), Error> {
    let exercise = conn.exercise(exercise_id).await?;
    if exercise.type_ != "programming" {
        return Err(Error::NotAProgrammingExercise {
            but: exercise.type_,
        });
    }

    let mut path = cfg.out_dir.clone();
    path.push(exercise.short_name.clone().unwrap());

    if path.exists() {
        return Ok(());
    }

    let student_participation: MaybeBorrowed<StudentParticipation>;
    match exercise.student_participations.len() {
        0 => student_participation = MaybeBorrowed::Owned(conn.participate(0, exercise_id).await?),
        1 => student_participation = MaybeBorrowed::Borrowed(&exercise.student_participations[0]),
        got => return Err(Error::AmbiguousParticipations { got }),
    };

    let git_url: String;
    if cfg.use_ssh {
        git_url = format!(
            "{}{}{}/{}.git",
            info.ssh_clone_url_template,
            exercise.course.as_ref().unwrap().short_name,
            exercise.short_name.clone().unwrap(),
            student_participation.build_plan_id
        )
    } else {
        git_url = student_participation.repository_url.to_owned();
    }

    let cloned_path = path.clone();
    let repo = task::spawn_blocking(move || {
        let mut callbacks = RemoteCallbacks::new();
        callbacks.credentials(credentials_callback);

        let mut fetch_options = FetchOptions::new();
        fetch_options.remote_callbacks(callbacks);

        RepoBuilder::new()
            .fetch_options(fetch_options)
            .clone(git_url.as_ref(), &cloned_path.clone())
    })
    .await??;

    if cfg.create_readme {
        path.push("README.md");
        let mut s = String::new();
        generate_readme(conn, &mut s, &exercise, Some(&student_participation)).await?;

        {
            let mut readme = File::create(&path).map_err(|err| Error::File {
                path: path.to_string_lossy().into_owned(),
                operation: "create",
                err,
            })?;
            readme.write_all(s.as_bytes()).map_err(|err| Error::File {
                path: path.to_string_lossy().into_owned(),
                operation: "write",
                err,
            })?;
        }

        if cfg.commit_initial {
            let sig = Signature::now(&cfg.committer_name, &cfg.committer_mail)?;

            let mut index = repo.index()?;
            index.add_path(&PathBuf::from("README.md"))?;
            let oid = index.write_tree()?;

            let latest_commit = repo
                .head()?
                .resolve()?
                .peel(ObjectType::Commit)?
                .into_commit()
                .expect("got empty repository");

            repo.commit(
                Some("HEAD"),
                &sig,
                &sig,
                "",
                &repo.find_tree(oid)?,
                &[&latest_commit],
            )?;
        }
    }

    if cfg.tag_initial {
        let sig = Signature::now(&cfg.committer_name, &cfg.committer_mail)?;
        let latest_commit = repo
            .head()?
            .resolve()?
            .peel(ObjectType::Commit)?
            .into_commit()
            .expect("got empty repository");
        repo.tag(
            "template",
            latest_commit.as_object(),
            &sig,
            "Blank template",
            false,
        )?;
    }

    for (name, base) in &cfg.remote_bases {
        repo.remote(
            name,
            &format!("{}{}.git", base, exercise.short_name.as_ref().unwrap()),
        )?;
    }

    Ok(())
}

fn credentials_callback(
    _url: &str,
    username_from_url: Option<&str>,
    allowed_types: CredentialType,
) -> Result<Cred, git2::Error> {
    if !allowed_types.contains(CredentialType::SSH_KEY) {
        return Cred::default();
    }
    if let Some(username_from_url) = username_from_url {
        Cred::ssh_key_from_agent(username_from_url)
    } else {
        Cred::default()
    }
}
