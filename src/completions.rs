use std::{
    fs,
    io::{self, Write},
    path::PathBuf,
};

use clap::{ArgEnum, IntoApp, Parser};
use clap_complete::{generate, Generator};

use crate::{Cli, Error};

/// Generate completion scripts for many shells.
#[derive(Debug, Parser)]
pub struct Completion {
    #[clap(short = 'i', long)]
    install_user: bool,

    #[clap(short = 'I', long)]
    install_system: bool,

    #[clap(arg_enum)]
    shell: Shell,
}

#[derive(Clone, Copy, Debug, ArgEnum)]
pub enum Shell {
    Bash,
    Elvish,
    Fish,
    Powershell,
    Zsh,
}

impl Shell {
    pub fn generator(self) -> clap_complete::shells::Shell {
        match self {
            Shell::Bash => clap_complete::shells::Shell::Bash,
            Shell::Elvish => clap_complete::shells::Shell::Elvish,
            Shell::Fish => clap_complete::shells::Shell::Fish,
            Shell::Powershell => clap_complete::shells::Shell::PowerShell,
            Shell::Zsh => clap_complete::shells::Shell::Zsh,
        }
    }

    pub fn user_completion_root(self) -> Option<PathBuf> {
        match self {
            Shell::Bash => {
                let mut d = dirs::data_dir()?;
                d.push("bash-completion");
                Some(d)
            }
            Shell::Elvish => None,
            Shell::Fish => {
                let mut d = dirs::config_dir()?;
                d.push("fish");
                d.push("completions");
                Some(d)
            }
            Shell::Powershell => None,
            Shell::Zsh => {
                let mut d = dirs::data_dir()?;
                d.push("zsh-functions");
                Some(d)
            }
        }
    }

    pub fn system_completion_root(self) -> Option<PathBuf> {
        match self {
            Shell::Bash => Some(PathBuf::from("/usr/share/bash-completion/completions")),
            Shell::Elvish => None,
            Shell::Fish => Some(PathBuf::from("/usr/local/share/fish/vendor_completions.d")),
            Shell::Powershell => None,
            Shell::Zsh => Some(PathBuf::from("/usr/share/zsh/site-functions")),
        }
    }
}

impl Completion {
    pub async fn main<'a>(&'a self, _cli: &'a Cli) -> Result<bool, Error> {
        let mut s = Vec::new();
        generate(
            self.shell.generator(),
            &mut Cli::into_app(),
            "pandia",
            &mut s,
        );

        let mut exit = true;
        if self.install_user {
            if let Some(mut root) = self.shell.user_completion_root() {
                if root.is_dir() {
                    root.push(self.shell.generator().file_name("pandia"));

                    fs::write(&root, &s).map_err(|err| Error::File {
                        path: root.to_string_lossy().to_string(),
                        operation: "write",
                        err,
                    })?;
                } else {
                    println!("Directory {:?} does not exist.\nCreate it and run again this command again.", root);
                    exit = false;
                }
            } else {
                println!(
                    "Automatic user installation is not available for {:?}.",
                    self.shell
                );
                exit = false;
            }
        }

        if self.install_system {
            if let Some(mut root) = self.shell.system_completion_root() {
                if root.is_dir() {
                    root.push(self.shell.generator().file_name("pandia"));
                    fs::write(&root, &s).map_err(|err| Error::File {
                        path: root.to_string_lossy().to_string(),
                        operation: "write",
                        err,
                    })?;
                } else {
                    println!("Directory {:?} does not exist.\nCreate it and run again this command again.", root);
                    exit = false;
                }
            } else {
                println!(
                    "Automatic system installation is not available for {:?}.",
                    self.shell
                );
                exit = false;
            }
        }

        if !self.install_system && !self.install_user {
            io::stdout().write_all(&s).unwrap();
        }

        Ok(exit)
    }
}
