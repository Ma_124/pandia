# Pandia
A CLI client for the [Artemis Platform](https://github.com/ls1intum/Artemis).
If you use IntelliJ and prefer a graphical tool, you can use [Orion](https://github.com/ls1intum/Orion/), developed at the same chair as Artemis.

## Table of contents
- [Installation](#installation)
- [Features](#features)
  - [Clone](#clone)
  - [Dashboard](#dashboard)
  - [Info](#info)
- [Example Config](#example-config)
- [License](#license)

## Installation
Build using `cargo`:
```bash
git clone https://gitlab.com/Ma_124/pandia.git && cd pandia
cargo build --locked --release
./target/release/pandia help
```

Or download a prebuilt binary:
- [Linux (glibc/x64)](https://gitlab.com/Ma_124/pandia/-/jobs/artifacts/master/raw/bin/pandia?job=build)

Then put the executable in a directory in your [`$PATH`](https://en.wikipedia.org/wiki/PATH_(variable)#Unix_and_Unix-like):
```bash
cp target/release/pandia ~/.local/bin
# or
cp target/release/pandia /usr/local/bin
```

Completion scripts for bash, elvish, fish, powershell, and zsh can be installed using one of the following commands:
```bash
pandia completion -i bash
sudo pandia completion -I bash
pandia completion bash > /your/completion/dir/pandia
```

## Features
### Clone
Run `pandia clone <course> <exercise>` (e.g. `pandia clone fpv21 w09h02`) to clone the exercise and do some setup tasks.

These are all opt-in and include:
1. Copying the exercise description from Artemis into a `README.md`.
2. Tag the commit of the blank exercise template for easy resetting.

### Dashboard
Run `pandia exercises` to see all upcoming exercises yielding points and their deadlines on one screen.
<!-- TODO add picture -->

Run `pandia exercises -t` to show tutorial exercises (and all other exercises yielding no points).

### Info
Run `pandia info` to show some info about the running Artemis instance.
```
$ pandia info
Artemis (de.tum.in.www1.artemis.Artemis) v5.4.4 built at 2022-01-07T10:51:01.008Z

Programming Languages:
  PYTHON
    Sequential Test Runs:  true
    Static Code Analysis:  false
    Plagiarism Check:      true
    Package Name Required: false

  C
    Sequential Test Runs:  false
    Static Code Analysis:  true
    Plagiarism Check:      true
    Package Name Required: false

  HASKELL
    Sequential Test Runs:  true
    Static Code Analysis:  false
    Plagiarism Check:      false
    Package Name Required: false

  JAVA
    Sequential Test Runs:  true
    Static Code Analysis:  true
    Plagiarism Check:      true
    Package Name Required: true
    Project Types:         ECLIPSE, MAVEN

[...]

SSH clone are relative to ssh://git@bitbucket.ase.in.tum.de:7999/

Browse VCS at https://bitbucket.ase.in.tum.de
Configure SSH keys at https://bitbucket.ase.in.tum.de/plugins/servlet/ssh/account/keys
```

## Example Config
Config Locations:
- Linux: `~/.config/pandia/config.toml`
- macOS: `~/Application Support/pandia/config.toml`
- Windows: `%AppData%/pandia/config.toml`

```toml
# URL to Artemis server.
artemis_url = 'https://artemis.ase.in.tum.de'

# The `dir` property in `[[courses]]` is relative to this.
base_path = '/some/base/directory'

# User data for first commit when `commit_initial` is set.
[committer]
name = "John Doe"
email = "info@example.org"

# All properties in the default block can be overriden in `[[courses]]` blocks.
[default]
# Clone using SSH instead of HTTPS
use_ssh = true

# Generate README.md using the exercise description.
create_readme = true

# Create an initial commit including the README.md.
commit_initial = true

# Tag the initial commit with `"template"` to allow easy resets.
tag_initial = true

# These definitions can be repeated for each course.
[[courses]]
# Name Artemis uses for this course.
# A list of all courses and their names can be found using `pandia courses`.
name = "pgdp1920"

# A directory relative to `base_path` in which the repositories will be cloned.
dir = "pgdp"

# Override property from `[default]` block.
tag_initial = false

[[courses]]
name = "artemistutorial"
# ...
```

Login Token Locations:
- Linux: `~/.cache/pandia.login`
- macOS: `~/Library/Caches/pandia.login`
- Windows: `%LocalAppData%/pandia.login`

Token files can be deleted using `pandia logout`.

## License
Copyright 2022 Ma\_124 \<ma_124+oss@pm.me\>

Licensed under either of
   Apache License, Version 2.0 or
   MIT License
at your option.

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
License, shall be dual licensed as above, without any additional terms
or conditions.
