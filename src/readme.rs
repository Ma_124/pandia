use crate::artemis::{ArtemisConnection, Exercise, StudentParticipation};
use crate::{format_datetime, Error};
use std::fmt::Write;

pub async fn generate_readme(
    conn: &ArtemisConnection,
    mut readme: impl Write,
    exercise: &Exercise,
    student_participation: Option<&StudentParticipation>,
) -> Result<(), Error> {
    let exercise = conn.exercise(exercise.id).await?;
    let mut lines = exercise.problem_statement.split('\n');

    // Write all line before initial heading (usually none)
    for s in lines.by_ref() {
        write!(readme, "{}", s.trim_end_matches('\r'))?;
        writeln!(readme)?;
        if s.starts_with('#') {
            break;
        }
    }

    write!(readme, "## Description")?;

    // Write lines and indent headings one step more (## -> ###)
    for mut s in lines {
        if s.starts_with('#') {
            write!(readme, "#")?;
        }

        const TASK_PREFIX: &str = "[task][";
        while let Some(task_index) = s.find(TASK_PREFIX) {
            // write everything before TASK_PREFIX to readme and remove it from s
            write!(readme, "{}", &s[..task_index])?;
            s = &s[task_index + TASK_PREFIX.len()..];

            write!(readme, "Test: ")?;

            // write everything between TASK_PREFIX and ] to readme and remove it from s
            let bracket_index = s.find(']').unwrap_or(s.len());
            write!(readme, "{}", &s[..bracket_index])?;
            s = &s[bracket_index + 1..];

            // remove everything between ] and ) from from s
            s = &s[s.find(')').unwrap_or(0) + 1..];
        }

        write!(readme, "{}", s.trim_end_matches('\r'))?;
        writeln!(readme)?;
    }

    writeln!(readme, "## Exercise Details")?;
    writeln!(readme, "|               | {:<80} |", "")?;
    writeln!(readme, "|---------------|-{:-<80}-|", "")?;
    if let Some(sn) = exercise.short_name {
        writeln!(readme, "| Short Name    | {:<80} |", sn)?;
    }
    if let Some(release_date) = exercise.release_date {
        writeln!(
            readme,
            "| Release date  | {:80} |",
            format_datetime(release_date),
        )?;
    } else {
        writeln!(readme, "| Release date  | {:80} |", "Unknown")?;
    }
    if let Some(due_date) = exercise.due_date {
        writeln!(
            readme,
            "| Due date      | {:80} |",
            format_datetime(due_date),
        )?;
    } else {
        writeln!(readme, "| Due date      | {:80} |", "None")?;
    }
    writeln!(readme, "| Max. Points   | {:<80} |", exercise.max_points)?;
    writeln!(readme, "| Bonus Points  | {:<80} |", exercise.bonus_points)?;
    writeln!(
        readme,
        "| Part of Score | {:<80} |",
        exercise.included_in_overall_score
    )?;
    writeln!(readme, "| Difficulty    | {:<80} |", exercise.difficulty)?;
    writeln!(readme, "| Mode          | {:<80} |", exercise.mode)?;
    writeln!(
        readme,
        "| Language      | {:<80} |",
        exercise.programming_language
    )?;
    writeln!(readme)?;

    let course = exercise.course.as_ref().unwrap();
    writeln!(readme, "## Course Details")?;
    writeln!(readme, "|               | {:<80} |", "")?;
    writeln!(readme, "|---------------|-{:-<80}-|", "")?;
    writeln!(readme, "| Title         | {:<80} |", course.title)?;
    writeln!(readme, "| Description   | {:<80} |", course.description)?;
    writeln!(readme, "| Semester      | {:<80} |", course.semester)?;
    writeln!(readme)?;
    writeln!(readme, "## Links")?;
    writeln!(
        readme,
        "- [Artemis]({}/courses/{}/exercises/{})",
        conn.base(),
        course.id,
        exercise.id
    )?;
    if let Some(student_participation) = student_participation {
        writeln!(
            readme,
            "- [Canonical VCS]({})",
            student_participation.repository_url
        )?;
    }

    Ok(())
}
