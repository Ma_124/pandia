use crate::artemis::{ArtemisConnection, AuthRequest};
use crate::config::Config;
use crate::{Cli, Error};
use clap::{ArgSettings, Parser, ValueHint};
use std::borrow::Cow;
use std::fs;
use std::fs::File;
use std::io::Write;

/// Log in to your Artemis account.
#[derive(Debug, Parser)]
pub struct Login {
    #[clap(value_hint = ValueHint::Other)]
    user: String,

    #[clap(env = "PANDIA_PASSWORD")]
    #[clap(value_hint = ValueHint::Other)]
    #[clap(setting = ArgSettings::HideEnvValues)]
    password: Option<String>,
}

impl Login {
    pub async fn main<'a>(&'a self, cli: &'a Cli) -> Result<bool, Error> {
        let cfg = Config::read(cli.config.clone())?;
        let conn = cfg.connect()?;
        if conn.token().is_some() {
            let account = conn.account().await?;
            println!(
                "Already logged in as {} <{}>. Logout first",
                account.name, account.email
            );
            return Ok(true);
        }

        let password: Cow<_> = if let Some(password) = &self.password {
            password.into()
        } else {
            rpassword::prompt_password_stdout("Password: ")
                .map_err(|err| Error::File {
                    path: "tty".to_owned(),
                    operation: "read password",
                    err,
                })?
                .into()
        };

        let conn = ArtemisConnection::authenticate(
            &cfg.artemis_url,
            AuthRequest {
                username: &self.user,
                password: &password,
                remember_me: true,
            },
        )
        .await?;

        let path = Config::login_path()?;
        let mut f = File::create(&path).map_err(|err| Error::File {
            path: path.to_string_lossy().into_owned(),
            operation: "create",
            err,
        })?;
        f.write_all(conn.token().unwrap().as_bytes())
            .map_err(|err| Error::File {
                path: path.to_string_lossy().into_owned(),
                operation: "write",
                err,
            })?;

        Ok(true)
    }
}

/// Log out of your Artemis account.
#[derive(Debug, Parser)]
pub struct Logout {}

impl Logout {
    pub async fn main<'a>(&'a self, _cli: &'a Cli) -> Result<bool, Error> {
        let path = Config::login_path()?;

        fs::remove_file(&path).map_err(|err| Error::File {
            path: path.to_string_lossy().into_owned(),
            operation: "remove",
            err,
        })?;
        Ok(true)
    }
}
