use crate::artemis::{ArtemisConnection, ArtemisError};
use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct AuthRequest<'a> {
    pub username: &'a str,
    pub password: &'a str,
    pub remember_me: bool,
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct AuthResponse {
    pub id_token: String,
}

impl ArtemisConnection {
    pub async fn authenticate(base: &str, req: AuthRequest<'_>) -> Result<Self, ArtemisError> {
        let mut conn = ArtemisConnection::new(base);
        let resp: AuthResponse = conn.http_post("/api/authenticate", &req).await?;
        conn.bearer_token = Some(Arc::new(resp.id_token));
        Ok(conn)
    }
}
