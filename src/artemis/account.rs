use crate::artemis::{ArtemisConnection, ArtemisError};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct Account {
    pub id: u32,
    pub login: String,
    pub name: String,
    pub email: String,
    pub lang_key: String,
    // ...
}

impl ArtemisConnection {
    pub async fn account(&self) -> Result<Account, ArtemisError> {
        self.http_get("/api/account").await
    }
}
