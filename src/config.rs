use crate::artemis::ArtemisConnection;
use crate::Error;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::fs::File;
use std::io::{ErrorKind, Read, Write};
use std::path::PathBuf;

#[derive(Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    pub artemis_url: String,
    pub base_path: String,

    pub committer: Committer,

    pub default: CommonCourseConfig,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub courses: Vec<CourseConfig>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            artemis_url: "https://artemis.ase.in.tum.de".to_string(),
            base_path: "".to_string(),
            committer: Committer {
                name: None,
                email: None,
            },
            default: CommonCourseConfig::global_default(),
            courses: Vec::default(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Committer {
    pub name: Option<String>,
    pub email: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CommonCourseConfig {
    pub use_ssh: Option<bool>,
    pub create_readme: Option<bool>,
    pub commit_initial: Option<bool>,
    pub tag_initial: Option<bool>,
}

macro_rules! accessors {
    ($($field: ident),+) => {
        $(
            pub fn $field(&self, default: &CommonCourseConfig) -> bool {
                self.$field.or(default.$field).or(Self::global_default().$field).unwrap_or_default()
            }
        )+
    };
}

impl CommonCourseConfig {
    pub fn global_default() -> Self {
        Self {
            use_ssh: Some(true),
            create_readme: Some(true),
            commit_initial: Some(false),
            tag_initial: Some(false),
        }
    }

    accessors!(use_ssh, create_readme, commit_initial, tag_initial);
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CourseConfig {
    pub name: String,
    pub dir: String,
    pub remote_bases: HashMap<String, String>,

    #[serde(flatten)]
    pub common: CommonCourseConfig,
}

impl Config {
    pub fn read(path: Option<PathBuf>) -> Result<Self, Error> {
        let config_path = Self::config_path(path.clone())?;

        let mut cfg = String::new();
        {
            let mut f = match File::open(&config_path) {
                Ok(f) => f,
                Err(err) if err.kind() == ErrorKind::NotFound => {
                    let cfg = Self::default();
                    cfg.write(path)?;
                    return Ok(cfg);
                }
                Err(err) => {
                    return Err(Error::File {
                        path: config_path.to_string_lossy().into_owned(),
                        operation: "open",
                        err,
                    })
                }
            };
            f.read_to_string(&mut cfg).map_err(|err| Error::File {
                path: config_path.to_string_lossy().into_owned(),
                operation: "read",
                err,
            })?;
        }

        toml::de::from_str(&cfg).map_err(|err| Error::Toml {
            path: config_path.to_string_lossy().into_owned(),
            err,
        })
    }

    pub fn write(&self, path: Option<PathBuf>) -> Result<(), Error> {
        let config_path = Self::config_path(path)?;

        let mut f = File::create(&config_path).map_err(|err| Error::File {
            path: config_path.to_string_lossy().into_owned(),
            operation: "create",
            err,
        })?;
        f.write(
            toml::ser::to_string_pretty(&self)
                .expect("toml ser error")
                .as_bytes(),
        )
        .map_err(|err| Error::File {
            path: config_path.to_string_lossy().into_owned(),
            operation: "write",
            err,
        })?;
        Ok(())
    }

    pub fn connect(&self) -> Result<ArtemisConnection, Error> {
        Ok(match File::open(Self::login_path()?) {
            Ok(mut f) => {
                let mut tok = String::new();
                if f.read_to_string(&mut tok).is_err() {
                    ArtemisConnection::new(&self.artemis_url)
                } else {
                    ArtemisConnection::from_token(&self.artemis_url, tok)
                }
            }
            Err(_) => ArtemisConnection::new(&self.artemis_url),
        })
    }

    pub fn config_path(path: Option<PathBuf>) -> Result<PathBuf, Error> {
        let mut config_path = match path {
            Some(path) => path,
            None => {
                let mut config_path = dirs::config_dir().expect("no home dir set");
                config_path.push("pandia");
                fs::create_dir_all(&config_path).map_err(|err| Error::File {
                    path: config_path.to_string_lossy().into_owned(),
                    operation: "mkdir",
                    err,
                })?;
                config_path
            }
        };

        config_path.push("config.toml");
        Ok(config_path)
    }

    pub fn login_path() -> Result<PathBuf, Error> {
        let mut cache = dirs::cache_dir().expect("no cache dir set");
        cache.push("pandia.login");
        Ok(cache)
    }
}
