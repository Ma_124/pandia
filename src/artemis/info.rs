use crate::artemis::{ArtemisConnection, ArtemisError};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct ArtemisInfo {
    pub build: ArtifactVersion,
    pub version_control_url: String,
    #[serde(rename = "commitHashURLTemplate")]
    pub commit_hash_url_template: String,
    #[serde(rename = "sshCloneURLTemplate")]
    pub ssh_clone_url_template: String,
    #[serde(rename = "sshKeysURL")]
    pub ssh_keys_url: String,
    pub programming_language_features: Vec<ProgrammingLanguageFeatures>, // ...
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct ArtifactVersion {
    pub artifact: String,
    pub name: String,
    pub time: String,
    pub version: String,
    pub group: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub struct ProgrammingLanguageFeatures {
    pub programming_language: String,
    pub sequential_test_runs: bool,
    pub static_code_analysis: bool,
    pub plagiarism_check_supported: bool,
    pub package_name_required: bool,
    pub checkout_solution_repository_allowed: bool,
    pub project_types: Vec<String>,
}

impl ArtemisConnection {
    pub async fn info(&self) -> Result<ArtemisInfo, ArtemisError> {
        self.http_get("/management/info").await
    }
}
