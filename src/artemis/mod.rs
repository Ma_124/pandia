// Unimplemented APIs:
// - Info about Artemis instance: https://artemis.ase.in.tum.de/management/info
// - Lecture: https://artemis.ase.in.tum.de/api/lectures/230
// - Courses for Notifications: https://artemis.ase.in.tum.de/api/courses/for-notifications
// - Notifications: https://artemis.ase.in.tum.de/api/notifications?page=0&size=25&sort=notificationDate,desc
// - Websocket: wss://artemis.ase.in.tum.de/websocket/tracker/*/websocket?access_token=[JWT]

use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::num::NonZeroU16;
use std::sync::Arc;
use thiserror::Error;

mod account;
mod authenticate;
mod courses;
mod info;

pub use account::*;
pub use authenticate::*;
pub use courses::*;
pub use info::*;

#[derive(Debug)]
pub struct ArtemisConnection {
    http: reqwest::Client,
    bearer_token: Option<Arc<String>>,
    base: Arc<str>,
}

impl ArtemisConnection {
    pub fn new(base: &str) -> Self {
        Self {
            http: reqwest::Client::builder()
                .user_agent(concat!(
                    "pandia/",
                    env!("CARGO_PKG_VERSION"),
                    " (+https://gitlab.com/Ma_124/pandia/-/issues)"
                ))
                .build()
                .expect("http client builder error"),
            bearer_token: None,
            base: Arc::from(base),
        }
    }

    pub fn from_token(base: &str, token: String) -> Self {
        let mut conn = Self::new(base);
        conn.bearer_token = Some(Arc::new(token));
        conn
    }

    pub fn base(&self) -> &str {
        &self.base
    }

    pub fn token(&self) -> Option<&str> {
        match &self.bearer_token {
            None => None,
            Some(tok) => Some(tok.as_str()),
        }
    }
}

impl ArtemisConnection {
    pub async fn time(&self) -> Result<String, ArtemisError> {
        self.http_get("/time").await
    }
}

impl ArtemisConnection {
    async fn do_http<T: DeserializeOwned>(
        &self,
        mut req_builder: reqwest::RequestBuilder,
        url: &str,
    ) -> Result<T, ArtemisError> {
        if let Some(token) = &self.bearer_token {
            req_builder = req_builder.bearer_auth(token);
        }
        let resp = req_builder.send().await.map_err(|err| ArtemisError::Http {
            err,
            url: url.to_owned(),
        })?;

        if resp.status().is_success() {
            resp.json().await.map_err(|err| ArtemisError::Http {
                err,
                url: url.to_owned(),
            })
        } else {
            Err(ArtemisError::Response {
                resp: resp.json().await.map_err(|err| ArtemisError::Http {
                    err,
                    url: url.to_owned(),
                })?,
                url: url.to_owned(),
            })
        }
    }

    pub async fn http_get<T: DeserializeOwned>(&self, url: &str) -> Result<T, ArtemisError> {
        self.do_http(self.http.get(format!("{}{}", self.base, url)), url)
            .await
    }

    pub async fn http_post<T: DeserializeOwned>(
        &self,
        url: &str,
        body: &impl Serialize,
    ) -> Result<T, ArtemisError> {
        self.do_http(
            self.http.post(format!("{}{}", self.base, url)).json(body),
            url,
        )
        .await
    }
}

#[derive(Debug, Error)]
pub enum ArtemisError {
    #[error("{url:?}: {err}")]
    Http { err: reqwest::Error, url: String },
    #[error("{url:?}: {} {}: {}", resp.status, resp.title, resp.detail)]
    Response {
        resp: ArtemisErrorResponse,
        url: String,
    },
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct ArtemisErrorResponse {
    #[serde(default)]
    pub title: String,
    pub status: NonZeroU16,
    #[serde(default)]
    pub detail: String,
    #[serde(default)]
    pub message: String,
}
