//! Younger sister of `ersa` in Rust.

use crate::account::{Login, Logout};
use crate::artemis::ArtemisError;
use crate::exercise::{Clone_, Exercise, Exercises};
use crate::info::{Courses, Info, Time, WhoAmI};
use ansi_term::Color;
use chrono::{DateTime, Duration, Local, TimeZone};
use clap::Parser;
use completions::Completion;
use std::fmt::Write;
use std::fmt::{self, Display};
use std::path::PathBuf;
use thiserror::Error;
use tokio::io;
use tokio::task::JoinError;

mod account;
mod artemis;
mod clone;
mod completions;
mod concurrent_stream;
mod config;
mod exercise;
mod info;
mod readme;

const COLOR_SHORT_NAME: Color = Color::Fixed(8);
const COLOR_DEADLINE_FAR: Color = Color::Green;
const COLOR_DEADLINE_CLOSE: Color = Color::Red;
const COLOR_DEADLINE_ENDED: Color = Color::Fixed(8);
const COLOR_TEAM: Color = Color::Cyan;

pub fn format_datetime<Z: TimeZone>(d: DateTime<Z>) -> impl Display
where
    DateTime<Local>: From<DateTime<Z>>,
{
    DateTime::<Local>::from(d).format("%Y-%m-%d T%H-%M-%S %:z")
}

pub fn format_datetime_locale<Z: TimeZone>(d: DateTime<Z>) -> impl Display
where
    DateTime<Local>: From<DateTime<Z>>,
{
    DateTime::<Local>::from(d).format("%c")
}

pub fn format_duration(d: Duration) -> impl Display {
    let mut s = String::new();
    if d >= Duration::days(1) {
        write!(&mut s, "{} day", d.num_days()).unwrap();

        if d >= Duration::days(2) {
            write!(&mut s, "s").unwrap();
        }
    }
    if d <= Duration::days(2) {
        write!(&mut s, " {} hours", d.num_hours() % 24).unwrap();
    }
    s
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("{0}")]
    Artemis(#[from] ArtemisError),
    #[error("git: {0}")]
    Git(#[from] git2::Error),
    #[error("thread {0}")]
    Join(#[from] JoinError),
    #[error("{0}")]
    Fmt(#[from] fmt::Error),
    #[error("{operation} {path:?}: {err}")]
    File {
        path: String,
        operation: &'static str,
        err: io::Error,
    },
    #[error("{path:?}: {err}")]
    Toml { path: String, err: toml::de::Error },
    #[error("ambiguous participations (got: {got})")]
    AmbiguousParticipations { got: usize },
    #[error("not a programming exercise but a {but} exercise")]
    NotAProgrammingExercise { but: String },
    #[error("unknown course {search}")]
    UnknownCourse { search: String },
    #[error("unknown exercise {search}")]
    UnknownExercise { search: String },
}

#[derive(Parser, Debug)]
#[clap(about, version, author)]
pub struct Cli {
    /// Configuration file
    #[clap(long = "config-dir")]
    pub config: Option<PathBuf>,

    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Parser)]
pub enum Commands {
    Info(Info),
    Login(Login),
    Logout(Logout),
    Courses(Courses),
    Time(Time),
    #[clap(name = "whoami")]
    WhoAmI(WhoAmI),
    Exercise(Exercise),
    Exercises(Exercises),
    Clone(Clone_),
    Completion(Completion),
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let cli: Cli = Cli::parse();

    if let Err(err) = match &cli.command {
        Commands::Info(c) => c.main(&cli).await,
        Commands::Login(c) => c.main(&cli).await,
        Commands::Logout(c) => c.main(&cli).await,
        Commands::Courses(c) => c.main(&cli).await,
        Commands::Time(c) => c.main(&cli).await,
        Commands::WhoAmI(c) => c.main(&cli).await,
        Commands::Exercise(c) => c.main(&cli).await,
        Commands::Clone(c) => c.main(&cli).await,
        Commands::Completion(c) => c.main(&cli).await,
        Commands::Exercises(c) => c.main(&cli).await,
    } {
        println!("Error: {}", err)
    }
}
