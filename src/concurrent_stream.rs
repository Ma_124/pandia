use futures::stream::FuturesUnordered;
use futures::{Future, Stream};
use std::pin::Pin;
use std::task::{Context, Poll};

/// Like [`FuturesUnordered`] but only poll at max `concurrency` [`Future`]s at once.
pub struct ConcurrentStream<Fut> {
    working: FuturesUnordered<Fut>,
    queued: Vec<Fut>,
}

impl<Fut> ConcurrentStream<Fut> {
    pub fn new(tasks: Vec<Fut>, concurrency: usize) -> Self {
        assert_ne!(concurrency, 0);
        let mut s = Self {
            working: FuturesUnordered::new(),
            queued: tasks,
        };
        for _ in 0..concurrency {
            if let Some(f) = s.queued.pop() {
                s.working.push(f)
            } else {
                debug_assert_eq!(s.queued.len(), 0);
                s.queued.shrink_to_fit();
                break;
            }
        }
        s
    }
}

impl<Fut: Future + Unpin> Stream for ConcurrentStream<Fut> {
    type Item = Fut::Output;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match Pin::new(&mut self.working).poll_next(cx) {
            Poll::Ready(Some(res)) => {
                if let Some(fut) = self.queued.pop() {
                    self.working.push(fut);

                    if self.queued.len() >= 8 && self.queued.len() <= self.queued.capacity() / 2 {
                        self.queued.shrink_to_fit();
                    }
                }
                Poll::Ready(Some(res))
            }
            Poll::Ready(None) => {
                debug_assert_eq!(self.queued.len(), 0);
                Poll::Ready(None)
            }
            Poll::Pending => Poll::Pending,
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.queued.len(), Some(self.queued.len()))
    }
}
